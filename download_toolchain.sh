#!/bin/bash

# Clean
rm -r -f armeb-linux-gnueabihf-linaro_7.1.1-2017.08
rm -r -fgcc-linaro-7.1.1-2017.08-x86_64_armeb-linux-gnueabihf
rm -f gcc-linaro-7.1.1-2017.08-x86_64_armeb-linux-gnueabihf.tar.xz

# Download Linaro 7.1.1
wget https://releases.linaro.org/components/toolchain/binaries/7.1-2017.08/armeb-linux-gnueabihf/gcc-linaro-7.1.1-2017.08-x86_64_armeb-linux-gnueabihf.tar.xz
 # Extract ToolChain
tar -xvf gcc-linaro-7.1.1-2017.08-x86_64_armeb-linux-gnueabihf.tar.xz
rm gcc-linaro-7.1.1-2017.08-x86_64_armeb-linux-gnueabihf.tar.xz

# Set Name
mv gcc-linaro-7.1.1-2017.08-x86_64_armeb-linux-gnueabihf armeb-linux-gnueabihf-linaro_7.1.1-2017.08
