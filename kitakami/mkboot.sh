#!/bin/bash
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
kernel_dir=$(pwd)
dir=$(pwd)/kitakami

#
# Touchwiz Kernel
#

# Create ramdisk.cpio.gz
cd $dir/Touchwiz/ramdisk
find ./ ! -name ramdisk.cpio.gz | cpio -H newc -o | gzip -9 > ramdisk.cpio.gz
mv ramdisk.cpio.gz ../ramdisk.cpio.gz
cd $dir

# Touchwiz Value
echo "Generating Touchwiz Boot Image"
./mkbootimg --kernel "$dir/Touchwiz/kernel" \
--ramdisk "$dir/Touchwiz/ramdisk.cpio.gz" \
--dt "$dir/Touchwiz/dt.img" \
--cmdline "console=null androidboot.hardware=qcom user_debug=31 msm_rtb.filter=0x3F androidboot.bootdevice=msm_sdcc.1 androidboot.selinux=permissive" \
--base 0x00000000 \
--pagesize 2048 \
--ramdisk_offset 0x02000000 \
--tags_offset 0x01e00000 \
--output $dir/Touchwiz/boot.img
cd $dir/Touchwiz
echo -n "SEANDROIDENFORCE" >> boot.img;

# Move Kernel
echo "Generating Kernel Flashing File"
cd $dir
mv Touchwiz/boot.img Lightning/boot.img

# Compression
cd $dir/Lightning
zip -r Lightning.zip ./*

# Move Kernel Flashing File
cd $dir/Lightning
mv Lightning.zip $kernel_dir/Lightning_ks01lteskt.zip
