#!/bin/bash

export ARCH=arm
#export CROSS_COMPILE=$(pwd)/arm-eabi-ubertc_7.0-2016.04/bin/arm-eabi- 
export CROSS_COMPILE=$(pwd)/armeb-linux-gnueabihf-linaro_7.1.1-2017.08/bin/armeb-linux-gnueabihf-

mkdir output

# Touchwiz Kernel
make -C $(pwd) O=output msm8974_sec_defconfig VARIANT_DEFCONFIG=msm8974_sec_ks01_skt_defconfig SELINUX_DEFCONFIG=selinux_defconfig
make -j8 -C $(pwd) O=output

cp output/arch/arm/boot/zImage $(pwd)/kitakami/Touchwiz/kernel

# Make Boot.img
./kitakami/mkboot.sh
