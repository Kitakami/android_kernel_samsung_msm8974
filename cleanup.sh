#!/bin/bash

# Clean Build Files
make mrproper
rm -r -f output/
rm -f kitakami/Touchwiz/boot.img
rm -f kitakami/Touchwiz/ramdisk.cpio.gz
rm -f kitakami/Lightning/boot.img
rm -f Lightning_ks01lteskt.zip
